package br.com.alura.rh.service;

import br.com.alura.rh.ValidacaoException;
import br.com.alura.rh.model.Funcionario;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Period;

public class ValidacaoPeridiocidadeEntreReajustes implements ValidacaoReajuste {
    @Override
    public void validar(Funcionario funcionario, BigDecimal aumento) {
        LocalDate dataUltimoReajuste = funcionario.getDataUltimoReajuste();
        LocalDate dataAtual = LocalDate.now();
        int months = Period.between(dataUltimoReajuste, dataAtual).getMonths();
        if (months < 6)
            throw new ValidacaoException("A data do reajuste anterior é inferior a 6 meses.");
    }
}
